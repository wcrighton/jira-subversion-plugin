package com.atlassian.jira.plugin.ext.subversion.revisions.scheduling;

import com.atlassian.jira.plugin.ext.subversion.MultipleSubversionRepositoryManager;
import com.atlassian.jira.plugin.ext.subversion.revisions.RevisionIndexer;
import com.atlassian.sal.api.scheduling.PluginJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;


public class UpdateIndexTask implements PluginJob {

    private final static Logger logger = LoggerFactory.getLogger(UpdateIndexTask.class);

    @Override
    public void execute(Map<String, Object> jobDataMap) {

        final UpdateIndexMonitorImpl monitor = (UpdateIndexMonitorImpl) jobDataMap.get("UpdateIndexMonitorImpl:instance");
        final MultipleSubversionRepositoryManager multipleSubversionRepositoryManager = (MultipleSubversionRepositoryManager) jobDataMap.get("MultipleSubversionRepositoryManager");
        assert monitor != null;

        try
        {

            if (null == multipleSubversionRepositoryManager)
            {
                return; // Just return --- the plugin is disabled. Don't log anything.
            }

            RevisionIndexer revisionIndexer = multipleSubversionRepositoryManager.getRevisionIndexer();
            if (revisionIndexer != null)
            {
                revisionIndexer.updateIndex();
            }
            else
            {
                logger.warn("Tried to index changes but SubversionManager has no revision indexer?");
            }
        }
        catch (Exception e)
        {
            logger.error("Error indexing changes: " + e);
        }

    }
}
