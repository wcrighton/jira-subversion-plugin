package com.atlassian.jira.plugin.ext.subversion.revisions.scheduling;

import com.atlassian.jira.InfrastructureException;
import com.atlassian.jira.plugin.ext.subversion.MultipleSubversionRepositoryManager;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import org.joda.time.DateTimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;

public class UpdateIndexMonitorImpl implements UpdateIndexMonitor, LifecycleAware, DisposableBean {
	
	private static final String JOB_NAME = UpdateIndexMonitorImpl.class.getName() + ":job";
    private final static Logger logger = LoggerFactory.getLogger(UpdateIndexMonitorImpl.class);
	private final PluginScheduler pluginScheduler;
    private final MultipleSubversionRepositoryManager multipleSubversionRepositoryManager;

    private static final long DEFAULT_INDEX_INTERVAL = DateTimeConstants.MILLIS_PER_HOUR;

	public UpdateIndexMonitorImpl(PluginScheduler pluginScheduler, MultipleSubversionRepositoryManager multipleSubversionRepositoryManager) {
		this.pluginScheduler = pluginScheduler;
        this.multipleSubversionRepositoryManager =  multipleSubversionRepositoryManager;
	}
	
	public void onStart() {
        schedule();
	}
	
	public void schedule() {

		pluginScheduler.scheduleJob(
                JOB_NAME,
                UpdateIndexTask.class,
                new HashMap<String, Object>() {{
                    put("UpdateIndexMonitorImpl:instance", UpdateIndexMonitorImpl.this);
                    put("MultipleSubversionRepositoryManager", multipleSubversionRepositoryManager);
                }},
                new Date(),
                DEFAULT_INDEX_INTERVAL);
        logger.info(String.format("UpdateIndexMonitorImpl scheduled to run every %dms", DEFAULT_INDEX_INTERVAL));
	}

    @Override
    public void destroy() {
        try
        {
            pluginScheduler.unscheduleJob(JOB_NAME);
        }
        catch (Exception e)
        {
            throw new InfrastructureException("Error unschedule update index job " + e);
        }
    }
}
